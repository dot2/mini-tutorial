//index.js
//获取应用实例
const app = getApp()



const html =
  `
  
  <h2 class="h2">What is Selenium</h2>
  
  <p>Selenium can be easily deployed on platforms such as Windows, Linux, Solaris and Macintosh. Moreover, it supports OS (Operating System) for mobile applications like iOS, windows mobile and android.</p> 
  <p>Selenium supports a variety of programming languages through the use of drivers specific to each language.Languages supported by Selenium include C#, Java, Perl, PHP, Python and Ruby.Currently, Selenium Web driver is most popular with Java and C#. Selenium test scripts can be coded in any of the supported programming languages and can be run directly in most modern web browsers. Browsers supported by Selenium include Internet Explorer, Mozilla Firefox, Google Chrome and Safari.</p>
  <img src="https://static.javatpoint.com/tutorial/selenium/images/selenium-what-is-selenium.png" alt="Selenium Tutorial What is Selenium">
  <p>Selenium can be used to automate functional tests and can be integrated with automation test tools such as <strong>Maven</strong>, <strong>Jenkins</strong>, <strong>&amp; Docker</strong> to achieve continuous testing. It can also be integrated with tools such as <strong>TestNG</strong>, &amp; <strong>JUnit</strong> for managing test cases and generating reports.</p>
  
`
Page({
  data: {
    html: html,
    nbLoading: false,
  },
  onLoad: function () {
    this.setData({
      nbTitle: 'c语言教程',
    })

    // 图片最大宽度为 100%
    this.setData({
      html: this.data.html.replace(/\<img/g, '<img style="max-width:100%; height:auto; display:block; " ')
    })
  },

  switchMenu: function () {
    this.setData({
      menuShow: !this.data.menuShow,
    })

  },
  switchBtnMenu: function () {
    this.setData({
      menuBtnShow: !this.data.menuBtnShow,
    })
  }
})